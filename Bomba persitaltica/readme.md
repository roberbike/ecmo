# Bomba Persitaltica #

## Descripción ##
Una bomba peristáltica es un tipo de bomba hidráulica de desplazamiento positivo usada para bombear una variedad de fluidos.


## Requerimientos funcionales ##
* Bombeo continuo
* Resolución de 1 ml
* Dirección hacia adelante / hacia atrás
* Velocidad ajustable
* 


## Hardware ##

El sitema mecanico consta de:
* Motor paso a paso
* Tubo OD 8mm
* 


El sitema electronico consta de:
* Arduino o un microprocesador similar
* Display (Pantalla LCD I2C 16x2 o OLED)
* Dos botones (?)
* Codificador rotatorio
* LEDs indicadores de estado (Verde: Operativo; Rojo: Alarma)


## Diseño actual (en desarollo) ##


## Como testear el bomba persitaltica
* Medición de consumo de corriente
* Encoder
* Sensor hall 


## Enlaces de interes ##
1. http://biohackacademy.github.io/biofactory/class/8-pumps/
2. http://2017.igem.org/Team:Aachen/Hardware

